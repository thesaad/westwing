//
//  Item.m
//  WestWing
//
//  Created by ews1 on 10/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "Item.h"
#import "WestWing+NSDictionary.h"


@implementation Item


+ (Item *)itemWithJsonDictionary:(NSDictionary *)dictionary
{
    
    if (!dictionary) {
        return nil;
    }
    
    Item *item = [[Item alloc] init];
    
    item.compainId = [[dictionary objectForKeyString:@"id_campaign"] longLongValue];
    item.name = [dictionary objectForKeyString:@"name"];
    item.subline = [dictionary objectForKeyString:@"subline"];
    item.desc = [dictionary objectForKeyString:@"description"];
    
    
    //shifted to images array. Wifht first index as navigation url, 2nd as banner, and next previews

//    item.navigation_url = [dictionary objectForKeyString:@"navigation_url"];
//    item.banner_url = [dictionary objectForKeyString:@"banner_url"];
    
    NSString *navigation_url = [dictionary objectForKeyString:@"navigation_url"];
    NSString *banner_url = [dictionary objectForKeyString:@"banner_url"];

    NSString *preview1 = [dictionary objectForKeyString:@"preview_image1_url"];
    NSString *preview2 = [dictionary objectForKeyString:@"preview_image2_url"];
    NSString *preview3 = [dictionary objectForKeyString:@"preview_image3_url"];
    
    item.images = @[navigation_url, banner_url, preview1, preview2, preview3];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    item.startTime = [formatter dateFromString:[dictionary objectForKeyString:@"start_time"]];
    item.endTime = [formatter dateFromString:[dictionary objectForKeyString:@"end_time"]];
    
    [formatter setDateFormat:@"dd.MM."];
    item.startTimeFormatted =[formatter dateFromString:[dictionary objectForKeyString:@"start_time_formatted"]];

    return item;
    
}

- (NSString *)formattedStartTimeSting
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM, dd"];
    return [formatter stringFromDate:self.startTimeFormatted];

}
- (BOOL)isEqual:(Item *)item2
{
    if (self.compainId == item2.compainId)
    {
        return true;
    }
    return  false;
}

@end
