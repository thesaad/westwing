//
//  Item.h
//  WestWing
//
//  Created by ews1 on 10/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (nonatomic, readwrite) long int compainId;
@property (nonatomic, readwrite) BOOL isFavorit;

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *subline;
@property (nonatomic, retain) NSString *desc;

@property (nonatomic, retain) NSArray *images;

@property (nonatomic, retain) NSDate *startTime;
@property (nonatomic, retain) NSDate *endTime;

@property (nonatomic, retain) NSDate *startTimeFormatted;


+ (Item *)itemWithJsonDictionary:(NSDictionary *)dict;

- (NSString *)formattedStartTimeSting;

@end
