//
//  Constants.m
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "Constants.h"

@implementation Constants

//Cell identifiers
NSString* const kItemsTableViewCellIdentifier                       =   @"ItemsTableViewCellIdentifier";


//Segue Identifers
NSString* const kItemsDetailViewControllerPushSegueIdentifer        =   @"ItemsDetailViewControllerPushSegueIdentifer";

@end
