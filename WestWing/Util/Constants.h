//
//  Constants.h
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject


#pragma mark - Cell identifiers
extern NSString* const kItemsTableViewCellIdentifier;


#pragma mark - Segue identifiers
extern NSString* const kItemsDetailViewControllerPushSegueIdentifer;


@end
