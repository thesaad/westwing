//
//  DetailViewController.h
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Item;

@interface DetailViewController : UIViewController

@property (nonatomic, retain) Item *selectedItem;

@end
