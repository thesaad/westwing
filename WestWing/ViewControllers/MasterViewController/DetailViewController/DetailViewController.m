//
//  DetailViewController.m
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "DetailViewController.h"
#import "JOLImageSlider.h"
#import "Item.h"
#import "MWPhotoBrowser.h"

@interface DetailViewController ()<JOLImageSliderDelegate, MWPhotoBrowserDelegate>
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *startinOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *sublineLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupImageSlider];
    [self populateInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Config
- (void)setupImageSlider
{
    NSMutableArray *slideSet = [NSMutableArray array];
    
    
    for (NSString *imgUrl in self.selectedItem.images)
    {
        JOLImageSlide *slide = [[JOLImageSlide alloc] init];
        
        slide.image = imgUrl;
        [slideSet addObject:slide];
    }
    
    
    JOLImageSlider *imageSlider = [[JOLImageSlider alloc] initWithFrame: CGRectMake(0, 64, self.view.frame.size.width, 140) andSlides: slideSet];
    
    imageSlider.delegate = self;
    
    imageSlider.autoSlide = false;
    imageSlider.placeholderImage = @"placeholder.png";
    imageSlider.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imageSlider];
    
}
#pragma mark - Populate info
- (void)populateInfo
{
    self.nameLabel.text = self.selectedItem.name;
    self.sublineLabel.text = self.selectedItem.subline;
    self.startinOnLabel.text = [self.selectedItem formattedStartTimeSting];
    self.descriptionTextView.text = self.selectedItem.desc;
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - JOLImageSliderDelegate
- (void)imagePager:(JOLImageSlider *)imagePager didSelectImageAtIndex:(NSUInteger)index
{
    //show image slider on selection of any thumbnail
    
}
- (void)showImageWithUrl:(NSString*)url atIndex:(NSUInteger)index
{
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    // Set options
    browser.displayActionButton = true;
    browser.displayNavArrows = false;
    browser.displaySelectionButtons = false;
    browser.zoomPhotosToFill = true;
    browser.alwaysShowControls = true;
    
    [browser setCurrentPhotoIndex:index];
    
    
    [self.navigationController pushViewController:browser animated:true];
    
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return self.selectedItem.images.count;
}
- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index;
{
    if (index < self.selectedItem.images.count)
    {
        MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:[self.selectedItem.images objectAtIndex:index]]];
        return photo;
    }
    return nil;
    
}


#pragma mark - DissmissView

- (IBAction)dismisDetailScreen:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:true];
}

@end
