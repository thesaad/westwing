//
//  ItemsTableViewCell.h
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Item;

@protocol ItemsTableViewCellDelegate;


@interface ItemsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *favButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sublineLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;

@property (assign) id<ItemsTableViewCellDelegate> delegate;

- (void)reset;
- (void)setItem:(Item *)item atIndexPath:(NSIndexPath *)indexPath;


@end


@protocol ItemsTableViewCellDelegate <NSObject>

- (void)itemsCell:(ItemsTableViewCell *)cell didTapFavoriteIconAtRow:(NSInteger)row;

@end