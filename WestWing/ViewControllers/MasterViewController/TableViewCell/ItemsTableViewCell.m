//
//  ItemsTableViewCell.m
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "ItemsTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Item.h"


@implementation ItemsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reset
{
    self.nameLabel.text = nil;
    self.sublineLabel.text = nil;
    self.favButton.selected = false;
    self.favButton.tag = -1;
    self.thumbnailImageView.image = nil;
    
    
    
}
- (void)setItem:(Item *)item atIndexPath:(NSIndexPath *)indexPath
{
    self.nameLabel.text = item.name;
    self.sublineLabel.text = item.subline;
    self.favButton.selected = item.isFavorit;
    self.favButton.tag = indexPath.row;
    
    [self.thumbnailImageView sd_setImageWithURL:[NSURL URLWithString:[item.images firstObject]] placeholderImage:[UIImage imageNamed:@"loading_text"]];
    

}

- (IBAction)favoritIcontapped:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(itemsCell:didTapFavoriteIconAtRow:)])
    {
        [self.delegate itemsCell:self didTapFavoriteIconAtRow:sender.tag];
    }
}
@end
