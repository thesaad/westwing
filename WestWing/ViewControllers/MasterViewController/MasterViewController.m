//
//  MasterViewController.m
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "MasterViewController.h"
#import "Constants.h"
#import "LGRefreshView.h"
#import "ItemsTableViewCell.h"
#import "APIController.h"
#import "Item.h"
#import "SVProgressHUD.h"
#import "DetailViewController.h"


@interface MasterViewController ()<UITableViewDelegate, UITableViewDataSource, ItemsTableViewCellDelegate>

@property (nonatomic, retain) LGRefreshView *refreshView;

@property (nonatomic, retain) NSMutableArray *itemsList;

@property (weak, nonatomic) IBOutlet UITableView *itemsTableView;


@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configreRefereshView];
    [self loadItemsFromNet];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIConfigurations
- (void)configreRefereshView
{

    
    self.refreshView = [LGRefreshView refreshViewWithScrollView:self.itemsTableView refreshHandler:^(LGRefreshView *refreshView) {
//        if (![self.refreshView isRefreshing])
//        {
            [self loadItemsFromNet];
//        }
        
    }];
    
}
#pragma mark - API Request

- (void)loadItemsFromNet
{
    [SVProgressHUD showWithStatus:@"Loading"];
    [[APIController sharedInstance] getItemsListWithResponseBlock:^(id object, BOOL status, NSError *error) {
       if (status && [object isKindOfClass:[NSArray class]])
       {
           
           self.itemsList = [NSMutableArray arrayWithArray:(NSArray *)object];
           [self.itemsTableView reloadData];
       }
        [_refreshView endRefreshing];
        [SVProgressHUD dismiss];


    }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:kItemsDetailViewControllerPushSegueIdentifer]) {
        NSIndexPath *indexPath = [self.itemsTableView indexPathForSelectedRow];
        
        if ([segue.destinationViewController isKindOfClass:[DetailViewController class]])
        {
            DetailViewController *detailVC = segue.destinationViewController;
            detailVC.selectedItem = [self.itemsList objectAtIndex:indexPath.row];
            
        }
    }
    
}


#pragma mark - TableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.itemsList.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItemsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kItemsTableViewCellIdentifier];
    [cell reset];
    [cell setItem:[self.itemsList objectAtIndex:indexPath.row] atIndexPath:indexPath];
    cell.delegate = self;
    return cell;
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to delete this object? This can't be undone." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            [self.itemsList removeObjectAtIndex:indexPath.row];
            
            [self.itemsTableView beginUpdates];
            [self.itemsTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.itemsTableView endUpdates];
            
        }]];
        
        [self presentViewController:alert animated:true completion:^{
            
        }];
        
    }
}
#pragma mark - Selection
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}



#pragma mark - ItemsTableViewCellDelegate
-(void)itemsCell:(ItemsTableViewCell *)cell didTapFavoriteIconAtRow:(NSInteger)row
{
    Item *item = [self.itemsList objectAtIndex:row];
    item.isFavorit = !item.isFavorit;
    [self.itemsTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    
}

@end
