//
//  APIController+Parser.h
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "APIController.h"


@interface APIController(Parser)

- (NSArray *)parseItemsFromResponse:(NSString *)response;

@end
