//
//  APIGateWay.h
//  WestWing
//
//  Created by ews1 on 10/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "JSONKit.h"
#import "WestWing+NSDictionary.h"

typedef void (^APIRequestResponseBlock) (id object, BOOL status, NSError *error);


@interface APIGateWay : NSObject


- (void)performOperation:(AFHTTPRequestOperation *)operation withSuccess:(void (^)())success andFailure:(void (^)(NSError*))failure;

@end
