//
//  APIRequest.h
//  WestWing
//
//  Created by ews1 on 10/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
// This class is configuration for request. It will have 4 methods for HTTP Methods GET, POST, PUT, DELETE

#import "APIGateWay.h"

@interface APIRequest : APIGateWay

- (void)makeGETRequestWithUrl:(NSString *)url andResponse:(void (^)(id, BOOL))responseBlock;

@end
