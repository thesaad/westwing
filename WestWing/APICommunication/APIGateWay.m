//
//  APIGateWay.m
//  WestWing
//
//  Created by ews1 on 10/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
// ALL API's will be passed through this class. we may set global variables here. like API Authorization token etc.


#import "APIGateWay.h"

@implementation APIGateWay

- (void)performOperation:(AFHTTPRequestOperation *)operation withSuccess:(void (^)())success andFailure:(void (^)(NSError*))failure
{
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary *jsonDictionary = [operation.responseString objectFromJSONString];
        if ([[jsonDictionary objectForKeyString:@"success"] boolValue] == true)
        {
            success();
        }
        else
        {
            // no iformation about json response internal_errror field
            failure(nil);
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    [operation start];

    
}

@end
