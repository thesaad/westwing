//
//  APIController.m
//  WestWing
//
//  Created by ews1 on 10/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "APIController.h"
#import "APIController+Parser.h"

@implementation APIController


+ (APIController *)sharedInstance
{
    static dispatch_once_t once;
    static APIController *controller;
    dispatch_once(&once, ^ { controller = [[APIController alloc] init]; });
    return controller;
}
- (id)init
{
    if ((self = [super init])) {
        // Initialization code.
        
    }
    return self;
}

- (void)getItemsListWithResponseBlock:(APIRequestResponseBlock)responseBlock
{
    NSString *url = @"http://www.westwing.uk/data.json";
    
    
    [self makeGETRequestWithUrl:url andResponse:^(id resoobseObject, BOOL status) {
        
        if (status && [resoobseObject isKindOfClass:[NSString class]]) {
            //success repsonse
            NSArray *items = [self parseItemsFromResponse:(NSString *)resoobseObject];
            
            responseBlock(items, true, nil);
            
        }
        else
        {
            responseBlock(nil, false, (NSError *)resoobseObject);
        }
    }];
}



    


@end
