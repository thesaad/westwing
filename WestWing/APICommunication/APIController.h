//
//  APIController.h
//  WestWing
//
//  Created by ews1 on 10/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
// This Class communicates with all other project classes for API communication

#import "APIRequest.h"

@interface APIController : APIRequest

+ (APIController *)sharedInstance;

- (void)getItemsListWithResponseBlock:(APIRequestResponseBlock)responseBlock;

@end
