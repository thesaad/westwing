//
//  APIController+Parser.m
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "APIController+Parser.h"
#import "Item.h"


@implementation APIController(Parser)

- (NSArray *)parseItemsFromResponse:(NSString *)response
{
    if (response == nil)
    {
        return nil;
    }
    NSMutableArray *itemsParsed = [NSMutableArray array];
    NSDictionary *jsonDictionary = [response objectFromJSONString];
    NSArray *itemsFromResponse = [[jsonDictionary objectForKeyString:@"metadata"] objectForKeyString:@"upcoming"];
    
    if ([itemsFromResponse isKindOfClass:[NSArray class]]) {
        //multiple objects
        for (NSDictionary* dict in itemsFromResponse) {
            
            Item *item = [Item itemWithJsonDictionary:dict];
            if (![itemsParsed containsObject:item])
                [itemsParsed addObject:[Item itemWithJsonDictionary:dict]];

            
        }
    }
    else if ([itemsFromResponse isKindOfClass:[NSDictionary class]])
    {
        // single object
        [itemsParsed addObject:[Item itemWithJsonDictionary:(NSDictionary *)itemsFromResponse]];

        
    }

    return itemsParsed;
}
@end
