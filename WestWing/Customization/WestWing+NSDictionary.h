//
//  WestWing+NSDictionary.h
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary(WestWing)

- (id)objectForKeyString:(NSString *)key;


@end
