//
//  WestWing+NSDictionary.m
//  WestWing
//
//  Created by ews1 on 11/08/2015.
//  Copyright (c) 2015 Saad. All rights reserved.
//

#import "WestWing+NSDictionary.h"

@implementation NSDictionary(WestWing)


- (id)objectForKeyString:(NSString *)key
{
    
    if ([[self objectForKey:key] isKindOfClass:[NSNull class]])
        
        return nil;
    
    return [self objectForKey:key];
    
}

@end
